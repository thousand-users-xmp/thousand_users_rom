class UserRepo < ROM::Repository[:users]
  commands :create, update: :by_pk

  def index
    users.select()
  end
end
