class UserRepo < ROM::Repository[:users]
    commands :create, update: :by_pk
end
