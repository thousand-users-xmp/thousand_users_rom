ROM::Rails::Railtie.configure do |config|
  config.gateways[:default] = [:sql, ENV.fetch('DATABASE_URL', 'postgres://127.0.0.1:5432/thousand_users_rom?user=maslenkov')]
end
