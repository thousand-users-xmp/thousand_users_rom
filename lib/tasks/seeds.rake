task seeding: [:environment] do
  user_repo = UserRepo.new(ROM.env)
  1000.times { |n| user_repo.create user_repo.changeset(email: "email#{n}@example.com").map(:add_timestamps) }
end
